#include <iostream>
#include <locale>

class MyClass
{
private:
    int a;
public:

    MyClass() : a(0)
    {}

    MyClass(int newA)
    {
        a = newA;
    }

    int GetA()
    {
        return a;
    }
};


class Vector
{
public:
    Vector() : x(10), y(10), z(10)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    int CalcModule()
    {
        VectorModule = sqrt(x*x + y*y + z*z);
        std::cout << '\n' << "������ �����: "  << VectorModule;
        return VectorModule;     
    }

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }
   
private:
    double x;
    double y;
    double z;
    double VectorModule = 0;
};


int main()
{
    setlocale(LC_ALL, "Russian");

    MyClass temp(11);
    std::cout << temp.GetA();

    Vector v;
    v.Show();
    v.CalcModule();
    
}

